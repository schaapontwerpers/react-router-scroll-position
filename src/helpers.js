/**
 * Scroll position of particular element
 *
 * @param {Object} elem - Element to get scroll position from
 *
 * @returns {int}
 */
export const getElemTop = (elem) => {
  let top = 0;

  // Check if element exists
  if (elem.nodeType === 1) {
    // Calculate top
    const box = elem.getBoundingClientRect();
    const { body } = document;

    // Get top
    const scrollTop = window.pageYOffset || document.scrollTop || body.scrollTop;
    const clientTop = document.clientTop || body.clientTop || 0;
    top = Math.round((box.top + scrollTop) - clientTop);

    // Loop over fixed elements to remove height from scroll position
    const fixedElems = document.querySelectorAll('[data-fixed]');

    if (fixedElems) {
      const fixedElemsLength = fixedElems.length;

      for (let i = 0; i < fixedElemsLength; i += 1) {
        const fixedElem = fixedElems[i];

        if (fixedElem.nodeType === 1) {
          top -= fixedElem.offsetHeight;
        }
      }
    }
  }

  return top;
};

/**
 * Run to set scroll to correct position
 *
 * @param {Object} history - History object from react router
 * @param {number} timeout - Timeout to use before scrolling
 *
 * @returns {void}
 */
export const setScrollPosition = (history, timeout = 2) => {
  const { hash } = window.location;

  let scroll = 0;

  // Check if it's pop action (back, forward, reload)
  if (history && history.action === 'POP') {
    // Get location key
    const { key } = history.location;

    // Set scroll position to location if there is a scroll position found in session
    if (sessionStorage.getItem(key)) {
      scroll = parseFloat(sessionStorage.getItem(key));
    }
  }

  // Check for hash location if scroll position is still 0
  if (hash && (scroll === 0)) {
    // Check if element with hash location exists
    const hashEl = document.getElementById(hash.replace('#', ''));

    if (hashEl) {
      scroll = getElemTop(hashEl);
    }
  }

  // Set scroll position correctly (small timeout for better reliability)
  setTimeout(() => {
    window.scrollTo(0, scroll);
  }, timeout);
};

/**
 * Get current scroll position
 *
 * @returns {int}
 */
export const getScrollPosition = () => (
  window.pageYOffset || document.scrollTop || document.body.scrollTop
);

/**
 * Store scroll position if location is switched
 *
 * @param {Object} location - Previous location
 * @param {int} scrollTop - scrollTop corresponding to the previous location
 *
 * @returns {void}
 */
export const storeScrollPosition = (location, scrollTop) => {
  const { key } = location;

  sessionStorage.setItem(key, scrollTop);
};
